using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ImageService.WebApi {
    public class Program {
        public static void Main(string[] args) {
            CreateHostBuilder(args)
                .Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureLogging((hosting, logging) => {
                    if (hosting.HostingEnvironment.IsProduction()) {
                        logging.ClearProviders();
                    }
                })
                .ConfigureWebHostDefaults(webBuilder => {
                    webBuilder
                        .UseKestrel(ops => ops.AddServerHeader = false)
                        .UseStartup<Startup>();
                });
    }
}
