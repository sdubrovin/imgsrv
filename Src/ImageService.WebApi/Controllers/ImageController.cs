using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using ImageService.Application.Images.Commands.Shared;
using ImageService.Application.Images.Commands.UploadImageByUri;
using ImageService.Application.Images.Commands.UploadImages;
using ImageService.Application.Images.Queries.GetImage;
using ImageService.Application.Images.Queries.GetImageDetails;
using ImageService.Application.Images.Queries.GetImageDetailsByUri;
using ImageService.Application.Images.Queries.Shared;
using ImageService.WebApi.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageService.WebApi.Controllers {
    [ApiController]
    public class ImageController : ControllerBase {
        private IMediator Mediator => HttpContext.RequestServices.GetService<IMediator>();

        [HttpGet]
        [ResponseCache(Location = ResponseCacheLocation.Any, Duration = 604800)]
        [Route("images/{id}")]
        [Route("images/{config}/{id}")]
        public async Task<IActionResult> GetImage(string id, string config) {
            var res = await Mediator.Send(new GetImageQuery { ImageId = id, ResizeConfig = config });
            return File(res.Data, res.MimeType);
        }

        [HttpPost]
        [Route("images")]
        public async Task<UploadImagesResult> UploadImage() {
            return await UploadPostedFiles();
        }

        [HttpPost]
        [Route("images/obsolete")]
        public async Task<object> UploadImageObsolete() {
            var result = await UploadPostedFiles();

            var res = new {
                isSuccess = true,
                images = result.Images.Select(i => i.Uri)
            };

            return res;
        }

        [HttpPost]
        [Route("images/uri")]
        public async Task<UploadedImage> UploadImageByUri([FromBody]UploadImageByUriCommand command) {
            return await Mediator.Send(command);
        }

        [HttpPost]
        [Route("images/storage")]
        public async Task<IEnumerable<ImageDetailsVm>> GetImageDetaildById([FromBody]GetImageDetailsQuery req) {
            return await Mediator.Send(req);
        }

        [HttpPost]
        [Route("images/storage/obsolete")]
        public async Task<IEnumerable<ImageDetailsVm>> GetImageDetails([FromBody]ImageDetailsRequest req) {
            var query = new GetImageDetailsByUriQuery { ImageUris = req.Images };
            return await Mediator.Send(query);
        }

        private async Task<UploadImagesResult> UploadPostedFiles() {
            var files = Request.Form.Files;

            var command = new UploadImagesCommand {
                Images = files?.Select(f => new UploadingImage {
                    FileName = f.FileName,
                    ContentType = f.ContentType,
                    Stream = f.OpenReadStream()
                })
            };

            return await Mediator.Send(command);
        }
    }
}
