using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ImageService.Application;
using ImageService.Infrastructure;
using ImageService.WebApi.Common;

namespace ImageService.WebApi {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }        

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            services.AddInfrastructure(Configuration, false);
            services.AddApplication();
            services.AddControllers();

            services.AddCors(options => {
                options.AddDefaultPolicy(p => {
                    p.WithOrigins("*");
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            IHostApplicationLifetime appLifetime
        ) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            app.UseInfrastructure(appLifetime);

            app.UseRouting();
            app.UseCors();
            app.UseAuthorization();
            app.UseCustomExceptionHandling();
            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
        }
    }
}
