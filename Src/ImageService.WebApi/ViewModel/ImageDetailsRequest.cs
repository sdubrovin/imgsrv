using System.Collections.Generic;

namespace ImageService.WebApi.ViewModel {
    public class ImageDetailsRequest {
        public IEnumerable<string> Images { get; set; }
    }
}
