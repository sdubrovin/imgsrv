using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using ImageService.Application.Common.Exceptions;

namespace ImageService.WebApi.Common {
    public class ExceptionHandlingMiddleware {
        private readonly RequestDelegate _next;

        public ExceptionHandlingMiddleware(RequestDelegate next) {
            _next = next;
        }

        public async Task Invoke(HttpContext context) {
            try {
                await _next(context);
            } catch (Exception ex) {
                var statusCode = HttpStatusCode.InternalServerError;

                switch (ex) {
                    case ImageNotFoundException _:
                        statusCode = HttpStatusCode.NotFound;
                        break;
                    case BadRequestException _:
                        statusCode = HttpStatusCode.BadRequest;
                        break;
                }

                context.Response.StatusCode = (int)statusCode;
                await context.Response.WriteAsync(ex.Message);
            }
        }
    }

    public static class ExceptionHandlingMiddlewareExtension {
        public static IApplicationBuilder UseCustomExceptionHandling(this IApplicationBuilder app) {
            app.UseMiddleware<ExceptionHandlingMiddleware>();
            return app;
        }
    }
}
