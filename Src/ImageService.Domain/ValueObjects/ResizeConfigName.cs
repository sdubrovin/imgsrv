namespace ImageService.Domain.ValueObjects {
    public class ResizeConfigName : Name {
        public ResizeConfigName(string name) : base(name) { }

        public static implicit operator string(ResizeConfigName obj) => obj?.Value;
        public static explicit operator ResizeConfigName(string str)
            => string.IsNullOrWhiteSpace(str) ? null : new ResizeConfigName(str);
    }
}
