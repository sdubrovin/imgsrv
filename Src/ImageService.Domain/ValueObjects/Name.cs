﻿using System;

namespace ImageService.Domain.ValueObjects {
    public abstract class Name {
        protected string Value { get; }

        public Name(string value) {
            if (string.IsNullOrWhiteSpace(value) || value.Contains(" ")) {
                throw new ArgumentException(nameof(value));
            }
            Value = value;
        }

        public override string ToString() => Value;
    }
}
