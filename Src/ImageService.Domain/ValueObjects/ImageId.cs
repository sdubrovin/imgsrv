﻿using System;

namespace ImageService.Domain.ValueObjects {
    public class ImageId : Name {
        public ImageId(string id) : base(id) { }
        public static implicit operator string(ImageId obj) => obj.Value;
        public static implicit operator ImageId(string id) => new ImageId(id);

        public static ImageId New() => new ImageId(Guid.NewGuid().ToString("N"));
    }
}
