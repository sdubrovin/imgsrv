using System;
using ImageService.Common;
using ImageService.Domain.ValueObjects;

namespace ImageService.Domain.Entities {
    public interface IImage {
        ImageId Id { get; }
        byte[] Data { get; }
        string DataHash { get; }
        string MimeType { get; }
        ResizeConfig ResizeConfig { get; }
        ImageSize Size { get; }
        DateTime CreationDate { get; }
        DateTime? DeleteAt { get; }
    }
}
