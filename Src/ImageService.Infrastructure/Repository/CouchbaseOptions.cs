using System;
using System.Collections.Generic;

namespace ImageService.Infrastructure.Repository {
    public class CouchbaseOptions {
        public List<Uri> Servers { get; set; }
        public string Bucket { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
