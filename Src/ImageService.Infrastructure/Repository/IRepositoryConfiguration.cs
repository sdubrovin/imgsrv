using System.Threading.Tasks;
using Couchbase.Core;

namespace ImageService.Infrastructure.Repository {
    interface IRepositoryConfiguration {
        Task Configure(IBucket bucket);
    }
}
