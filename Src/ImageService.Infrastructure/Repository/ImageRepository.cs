using Couchbase.Core;
using Couchbase.Linq;
using Couchbase.N1QL;
using ImageService.Application.Common.Repository;
using ImageService.Common;
using ImageService.Domain.Entities;
using ImageService.Domain.ValueObjects;
using ImageService.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageService.Infrastructure.Repository {
    public class ImageRepository : IImageRepository, IRepositoryConfiguration {
        private readonly IBucketContext _bucketContext;

        public ImageRepository(IBucketContext bucket) {
            _bucketContext = bucket;
        }

        public async Task<IImage> GetImageById(ImageId id, ResizeConfig resizeConfig) {
            var res = await _bucketContext.Bucket.GetAsync<Image>(GetKey(id, resizeConfig));
            return res.Value;
        }

        public async Task<IImage> GetImageByHash(string dataHash) {
            var res = from i in _bucketContext.Query<Image>()
                      where i.DataHash == dataHash
                      select i;

            return await Task.FromResult(res.FirstOrDefault());
        }

        public Task SaveImage(IImage image) => 
            _bucketContext.Bucket.UpsertAsync(GetKey(image), (Image)image, GetExpiration(image.DeleteAt));

        private string GetKey(IImage image) => GetKey(image.Id, image.ResizeConfig);

        private string GetKey(ImageId id, ResizeConfig config) {
            var key = $"Image-{id}";

            if (config != null) {
                key += $"-{config.Width}x{config.Height}-{config.Align}-{config.Fill}";
            }

            return key;
        }

        private uint GetExpiration(DateTime? date) {
            if (date is null) {
                return 0;
            }

            return (uint)date.Value.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }

        public async Task Configure(IBucket bucket) {
            var indexList = await GetIndexList(bucket);
            
            if (!indexList.Contains("idx_image_dataHash")) {
                var query = $"CREATE INDEX idx_image_dataHash ON {bucket.Name}(DataHash) " +
                            $"WHERE Type = \"Image\" AND DataHash IS VALUED USING GSI";

                await bucket.QueryAsync<object>(new QueryRequest(query));
            }

            if (!indexList.Contains("idx_image_creationDate")) {
                var query = $"CREATE INDEX idx_image_creationDate ON {bucket.Name}(CreationDate) " +
                            $"WHERE Type = \"Image\" AND DataHash IS VALUED USING GSI";

                await bucket.QueryAsync<object>(new QueryRequest(query));
            }
        }

        private async Task<IEnumerable<string>> GetIndexList(IBucket bucket) {
            var query = $"select raw name from system:indexes where keyspace_id = \"{bucket.Name}\"";
            var res = await bucket.QueryAsync<string>(new QueryRequest(query));
            return res.ToList();
        }
    }
}
