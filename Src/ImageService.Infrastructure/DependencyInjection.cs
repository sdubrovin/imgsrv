using System;
using System.Net.Http.Headers;
using System.Text;
using Couchbase;
using Couchbase.Authentication;
using Couchbase.Configuration.Client;
using Couchbase.Core.Serialization;
using Couchbase.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using ImageService.Application.Common.Repository;
using ImageService.Application.Common.Services;
using ImageService.Common;
using ImageService.Infrastructure.JsonConverters;
using ImageService.Infrastructure.Repository;
using ImageService.Infrastructure.Services;

namespace ImageService.Infrastructure {
    public static class DependencyInjection {
        public static IServiceCollection AddInfrastructure(
            this IServiceCollection services,
            IConfiguration configuration,
            bool isProduction
        ) {
            var couchbaseOptions = new CouchbaseOptions();
            configuration.GetSection("Couchbase").Bind(couchbaseOptions);

            var clientConfig = new ClientConfiguration {
                Servers = couchbaseOptions.Servers,
                Serializer = () => {
                    var serializerSettings = new JsonSerializerSettings {
                        ContractResolver = new DefaultContractResolver()
                    };
                    serializerSettings.Converters.Add(new ImageIdConverter());
                    serializerSettings.Converters.Add(new StringEnumConverter());
                    return new DefaultSerializer(serializerSettings, serializerSettings);
                }
            };
            var auth = new PasswordAuthenticator(couchbaseOptions.UserName, couchbaseOptions.Password);
            ClusterHelper.Initialize(clientConfig, auth);
            var bucket = ClusterHelper.GetBucket(couchbaseOptions.Bucket);

            services.AddSingleton(bucket);
            services.AddTransient<IBucketContext>(c => new BucketContext(bucket));
            services.AddTransient<IImageRepository, ImageRepository>();
            services.AddTransient<IRepositoryConfiguration, ImageRepository>();
            services.AddHttpClient();

            if (isProduction) {
                var configProviderOptions = new WebResizeConfigProviderOptions();
                configuration.GetSection("ResizeConfigProvider").Bind(configProviderOptions);

                services.AddTransient(c => configProviderOptions);
                services.AddHttpClient<IResizeConfigProvider, WebResizeConfigProvider>(c => {
                    var value = Convert.ToBase64String(
                        Encoding.ASCII.GetBytes($"{configProviderOptions.Login}:{configProviderOptions.Password}")
                    );
                    c.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",value);
                });
            } else {
                services.AddTransient<IResizeConfigProvider, DummyResizeConfigProvider>();
            }
            services.AddTransient<IImageUriService, ImageUriService>(с => {
                var baseUri = new Uri(configuration["BaseUri"]);
                return new ImageUriService(baseUri);
            });
            services.AddTransient<IImageSizeCalculator, ImageSizeCalculator>();
            services.AddTransient<IImageResizer, ImageResizer>();
            services.AddTransient<IHashCalculator, HashCalculator>();
            services.AddTransient<IImageService, Services.ImageService>();

            var serviceProvider = services.BuildServiceProvider();
            var repoConfigurators = serviceProvider.GetServices<IRepositoryConfiguration>();

            foreach (var repoConfig in repoConfigurators) {
                repoConfig.Configure(bucket);
            }

            return services;
        }

        public static IApplicationBuilder UseInfrastructure(
            this IApplicationBuilder builder,
            IHostApplicationLifetime appLifetime
        ) {
            appLifetime.ApplicationStopped.Register(OnStopped);
            return builder;
        }

        private static void OnStopped() {
            ClusterHelper.Close();
        }
    }
}
