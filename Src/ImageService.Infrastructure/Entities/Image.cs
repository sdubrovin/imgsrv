using System;
using Couchbase.Linq.Filters;
using ImageService.Common;
using ImageService.Domain.Entities;
using ImageService.Domain.ValueObjects;

namespace ImageService.Infrastructure.Entities {
    [DocumentTypeFilter(nameof(Image))]
    public class Image : IImage {
        public string Type => nameof(Image);
        public ImageId Id { get; set; }
        public string MimeType { get; set; }
        public string DataHash { get; set; }
        public ImageSize Size { get; set; }
        public ResizeConfig ResizeConfig { get; set; }
        public byte[] Data { get; set; }
        public DateTime? DeleteAt { get; set; }
        public DateTime CreationDate { get; set; } = DateTime.UtcNow;

        public Image(
            ImageId id,
            string mimeType,
            ImageSize size,
            ResizeConfig resizeConfig,
            byte[] data,
            string hash,
            DateTime? creationDate = null,
            DateTime? deleteAt = null
        ) {
            Id = id;
            MimeType = mimeType;
            Size = size;
            Data = data;
            DataHash = hash;
            ResizeConfig = resizeConfig;
            CreationDate = creationDate.GetValueOrDefault(DateTime.UtcNow);
            DeleteAt = deleteAt;
        }
    }
}
