using System;
using ImageService.Application.Common.Services;
using ImageService.Domain.Entities;

namespace ImageService.Infrastructure.Services {
    public class ImageUriService : IImageUriService {
        private readonly Uri _baseUri;

        public ImageUriService(Uri baseUri) {
            _baseUri = baseUri;
        }

        public string GetImageUri(IImage image) {
            string path = "/images";

            if (image.ResizeConfig != null) {
                path += $"/{image.ResizeConfig.Name}";
            }
            path += $"/{image.Id}";

            return new Uri(_baseUri, path).ToString();
        }
    }
}
