using System;
using System.Security.Cryptography;
using System.Threading.Tasks;
using ImageService.Application.Common.Services;

namespace ImageService.Infrastructure.Services {
    public class HashCalculator : IHashCalculator {
        public Task<string> GetHash(byte[] bytes) {
            using (var md5 = MD5.Create()) {
                var hashBytes = md5.ComputeHash(bytes);
                var hash = BitConverter
                    .ToString(hashBytes)
                    .Replace("-", string.Empty)
                    .ToLowerInvariant();

                return Task.FromResult(hash);
            }
        }
    }
}
