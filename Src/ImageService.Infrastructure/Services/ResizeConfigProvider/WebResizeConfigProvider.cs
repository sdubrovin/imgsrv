using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ImageService.Common;

namespace ImageService.Infrastructure.Services {
    public class WebResizeConfigProvider : IResizeConfigProvider {
        private static bool _loaded = false;
        private static ResizeConfigVm _resizeConfigVm;

        private readonly WebResizeConfigProviderOptions _options;
        private readonly HttpClient _httpClient;

        public WebResizeConfigProvider(WebResizeConfigProviderOptions options, HttpClient httpClient) {
            _options = options;
            _httpClient = httpClient;
        }

        public async Task<ResizeConfig> GetResizeConfig(string name) {
            if (!_loaded) {
                await Reload();
                _loaded = true;
            }

            return _resizeConfigVm.Configs.FirstOrDefault(c => string.Compare(c.Name, name, true) == 0);
        }

        public async Task Reload() {
            using (var response = await _httpClient.GetAsync(_options.Uri)) {
                response.EnsureSuccessStatusCode();
                var responseContent = await response.Content.ReadAsStringAsync();
                _resizeConfigVm = JsonConvert.DeserializeObject<ResizeConfigVm>(responseContent);
            }
        }

        private class ResizeConfigVm {
            public List<ResizeConfig> Configs { get; set; }
        }
    }
}
