using ImageService.Common;
using System.Threading.Tasks;

namespace ImageService.Infrastructure.Services {
    public interface IResizeConfigProvider {
        Task<ResizeConfig> GetResizeConfig(string name);
        Task Reload();
    }
}
