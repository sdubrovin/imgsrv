using System;

namespace ImageService.Infrastructure.Services {
    public class WebResizeConfigProviderOptions {
        public Uri Uri { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
