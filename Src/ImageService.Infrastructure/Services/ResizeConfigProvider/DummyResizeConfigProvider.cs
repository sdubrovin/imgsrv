using ImageService.Common;
using System.Threading.Tasks;

namespace ImageService.Infrastructure.Services {
    public class DummyResizeConfigProvider : IResizeConfigProvider {
        public Task<ResizeConfig> GetResizeConfig(string name) {
            var res = new ResizeConfig {
                Align = AlignType.Center,
                Name = "DummyResizeConfig",
                Fill = FillType.UniformToFill,
                Height = 100,
                Width = 100
            };
            return Task.FromResult(res);
        }

        public Task Reload() => Task.CompletedTask;
    }
}
