using ImageService.Application.Common.Exceptions;
using ImageService.Application.Common.Repository;
using ImageService.Application.Common.Services;
using ImageService.Common;
using ImageService.Domain.Entities;
using ImageService.Domain.ValueObjects;
using ImageService.Infrastructure.Entities;
using System;
using System.IO;
using System.Threading.Tasks;

namespace ImageService.Infrastructure.Services {
    public class ImageService : IImageService {
        private readonly static TimeSpan ResizedImageLifetime = TimeSpan.FromDays(30);
        private readonly IImageRepository _imageRepository;
        private readonly IResizeConfigProvider _resizeConfigProvider;
        private readonly IHashCalculator _hashCalculator;
        private readonly IImageResizer _imageResizer;

        public ImageService(
            IImageRepository imageRepository,
            IResizeConfigProvider resizeConfigProvider,
            IHashCalculator hashCalculator,
            IImageResizer imageResizer
        ) {
            _imageRepository = imageRepository;
            _resizeConfigProvider = resizeConfigProvider;
            _hashCalculator = hashCalculator;
            _imageResizer = imageResizer;
        }

        public async Task<IImage> GetImage(ImageId imageId, ResizeConfigName configName) {
            ResizeConfig resizeConfig = null;

            if (configName != null) {
                resizeConfig = await _resizeConfigProvider.GetResizeConfig(configName);
            }

            var image = await _imageRepository.GetImageById(imageId, resizeConfig);

            if (image is null && configName is null) {
                throw new ImageNotFoundException(imageId);
            }

            if (image?.ResizeConfig != resizeConfig) {
                image = null;
            }

            return image ?? await ResizeImage(imageId, resizeConfig);
        }

        public async Task<IImage> SaveImage(string contentType, Stream stream) {
            var imageBytes = ToByteArray(stream);
            var imageHash =  await _hashCalculator.GetHash(imageBytes);
            var image = await _imageRepository.GetImageByHash(imageHash);

            if (image is null) {
                image = new Image(
                    ImageId.New(),
                    contentType,
                    GetImageSize(stream),
                    null,
                    imageBytes,
                    imageHash
                );
                await _imageRepository.SaveImage(image);
            }

            return image;
        }

        private async Task<IImage> ResizeImage(ImageId imageId, ResizeConfig resizeConfig) {
            var originalImage = await _imageRepository.GetImageById(imageId, null);

            if (originalImage is null) {
                throw new ImageNotFoundException(imageId);
            }

            if (resizeConfig is null) {
                return originalImage;
            }

            byte[] imageBytes = _imageResizer.Resize(
                originalImage.Data,
                originalImage.MimeType,
                resizeConfig
            );

            var resizedImage = new Image(
               imageId,
               originalImage.MimeType,
               GetImageSize(imageBytes),
               resizeConfig,
               imageBytes,
               null,
               deleteAt: DateTime.UtcNow.Add(ResizedImageLifetime)
            );
            await _imageRepository.SaveImage(resizedImage);

            return resizedImage;
        }

        private byte[] ToByteArray(Stream stream) {
            stream.Position = 0;
            using (MemoryStream memoryStream = new MemoryStream()) {
                stream.CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }

        private ImageSize GetImageSize(Stream stream) {
            stream.Position = 0;
            using (var img = System.Drawing.Image.FromStream(stream)) {
                return new ImageSize(img.Width, img.Height);
            }
        }

        private ImageSize GetImageSize(byte[] imageBytes) {
            using (var memoryStream = new MemoryStream(imageBytes)) {
                return GetImageSize(memoryStream);
            }
        }
    }
}
