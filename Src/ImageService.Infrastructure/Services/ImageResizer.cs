using ImageService.Application.Common.Services;
using ImageService.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace ImageService.Infrastructure.Services {
    public class ImageResizer : IImageResizer {
        private readonly static Dictionary<string, ImageCodecInfo> CodecInfoByMimeType;
        private readonly IImageSizeCalculator _imageSizeCalculator;

        static ImageResizer() {
            var decoders = ImageCodecInfo.GetImageDecoders();
            CodecInfoByMimeType = new Dictionary<string, ImageCodecInfo> {
                { "image/jpeg", decoders.Single(x => x.FormatID == ImageFormat.Jpeg.Guid) },
                { "image/png", decoders.Single(x => x.FormatID == ImageFormat.Png.Guid) }
            };
        }

        public ImageResizer(IImageSizeCalculator imageSizeCalculator) {
            _imageSizeCalculator = imageSizeCalculator;
        }

        public byte[] Resize(
            byte[] imageBytes,
            string mimeType,
            ResizeConfig config
        ) {
            var encoder = GetCodecInfo(mimeType);
            using (var inputStream = new MemoryStream(imageBytes)) {
                using (var sourceImage = new Bitmap(inputStream)) {
                    var img = ResizeImage(sourceImage, config);
                    img = Crop(img, config);

                    var myEncoderParameters = new EncoderParameters(1);
                    var myEncoderParameter = new EncoderParameter(Encoder.Quality, 100L);
                    myEncoderParameters.Param[0] = myEncoderParameter;

                    using (var outputStream = new MemoryStream()) {
                        img.Save(outputStream, encoder, myEncoderParameters);
                        return outputStream.ToArray();
                    }
                }
            }
        }

        private Rectangle? CreateCropRectangle(int imageWidth, int imageHeight, ResizeConfig config) {
            if (imageWidth == config.Width && imageHeight == config.Height) {
                //NOTE: it means original image
                return null;
            }

            int width = Math.Min(imageWidth, config.Width);
            int height = Math.Min(imageHeight, config.Height);

            switch (config.Align) {
                case AlignType.TopOrLeft:
                    return new Rectangle(0, 0, width, height);
                case AlignType.Center:
                    int x = (imageWidth - width) / 2;
                    int y = (imageHeight - height) / 2;
                    return new Rectangle(x, y, width, height);
                default:
                    return null;
            }
        }

        private Image Crop(Image img, ResizeConfig resizeConfig) {
            var bitmap = new Bitmap(img);

            Rectangle? cropArea = CreateCropRectangle(img.Width, img.Height, resizeConfig);

            return cropArea.HasValue ? bitmap.Clone(cropArea.Value, bitmap.PixelFormat) : img;
        }

        private Image ResizeImage(Image image, ResizeConfig resizeConfig) {
            var size = _imageSizeCalculator.GetImageSize(
                new ImageSize(image.Width, image.Height),
                resizeConfig
            );

            var bitmap = new Bitmap(size.Width, size.Height);
            using (Graphics graphics = Graphics.FromImage(bitmap)) {
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.DrawImage(image, 0, 0, size.Width, size.Height);
            }

            return bitmap;
        }

        private ImageCodecInfo GetCodecInfo(string mimeType) {
            if (!CodecInfoByMimeType.TryGetValue(mimeType, out var result)) {
                throw new Exception($"MimeType {mimeType} not supported.");
            }

            return result;
        }
    }
}
