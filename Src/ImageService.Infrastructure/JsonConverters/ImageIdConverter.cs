﻿using Newtonsoft.Json;
using ImageService.Domain.ValueObjects;
using System;

namespace ImageService.Infrastructure.JsonConverters {
    public class ImageIdConverter : JsonConverter<ImageId> {
        public override ImageId ReadJson(JsonReader reader, Type objectType, ImageId existingValue, bool hasExistingValue, JsonSerializer serializer) {
            string str = (string)reader.Value;
            return new ImageId(str);
        }

        public override void WriteJson(JsonWriter writer, ImageId value, JsonSerializer serializer) {
            serializer.Serialize(writer, value.ToString());
        }
    }
}
