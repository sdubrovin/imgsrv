using System;

namespace Radario.ImageService.Migrator {
    public class RadarioImage {
        public string Type { get; set; }
        public int EntityId { get; set; }
        public string Id { get; set; }
        public string Url => $"http://old-images.radario.ru/images/{Id}";

        public override bool Equals(object obj) {
            return obj is RadarioImage image &&
                   Id == image.Id;
        }

        public override int GetHashCode() {
            return HashCode.Combine(Id);
        }
    }
}
