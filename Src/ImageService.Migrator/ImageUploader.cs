using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Radario.ImageService.Migrator {
    public class ImageUploader {
        private const int BathSize = 10;
        private readonly HttpClient _httpClient;

        public ImageUploader(HttpClient httpClient) {
            _httpClient = httpClient;
        }

        public async Task Upload(IEnumerable<RadarioImage> images, MigrationReport report) {
            IEnumerable<Task> tasks = Enumerable.Empty<Task>();
            int skip = 0;

            Console.WriteLine($"Uploading {images.Count()} images");
            do {
                tasks = images.Skip(skip).Take(BathSize).Select(async img => await UploadImage(img, report));
                skip += BathSize;
                await Task.WhenAll(tasks);
            } while (tasks.Any());
            Console.WriteLine("Done");
            Console.WriteLine();
        }

        private async Task UploadImage(RadarioImage img, MigrationReport report) {
            try {
                var json = JsonSerializer.Serialize(img);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                using var response = await _httpClient.PostAsync("/images/uri", content);
                response.EnsureSuccessStatusCode();
            } catch (Exception ex) {
                report.FailedImages.Add(new FailedImage {
                    Image = img,
                    Error = ex.Message
                });
            }
        }
    }
}
