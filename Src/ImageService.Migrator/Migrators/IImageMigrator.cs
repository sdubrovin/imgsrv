﻿using System.Threading.Tasks;

namespace Radario.ImageService.Migrator {
    public interface IImageMigrator {
        Task<MigrationReport> Migrate();
    }
}