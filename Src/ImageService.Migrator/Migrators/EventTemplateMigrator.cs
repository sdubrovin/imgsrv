using System.Collections.Generic;

namespace Radario.ImageService.Migrator {
    public class EventTemplate: BaseEntity {
        public string Images { get; set; }
        public string BackgroundImage { get; set; }
        public string PlaceSchemeImage { get; set; }
    }

    public class EventTemplateMigrator : BaseImageMigrator<EventTemplate> {
        public EventTemplateMigrator(string connectionString, ImageUploader imageUploader) :
            base(nameof(EventTemplate), connectionString, imageUploader) { }

        protected override IEnumerable<RadarioImage> ExtractImage(IEnumerable<EventTemplate> entities) {
            var images = new List<RadarioImage>();

            images.AddRange(
                GetImages("EventTemplate.Images", entities, e => ExtractImageList(e.Images))
            );
            images.AddRange(
                GetImages("EventTemplate.BackgroundImage", entities, e => ExtractOneImage(e.BackgroundImage))
            );
            images.AddRange(
                GetImages("EventTemplate.PlaceSchemeImage", entities, e => ExtractOneImage(e.PlaceSchemeImage))
            );

            return images;
        }

        protected override string GetEntititesQuery(int fromId, int count) {
            return $"SELECT TOP (@count) Id, Images, BackgroundImage, PlaceSchemeImage FROM [EventTemplate] " +
                   $"WHERE Id > @fromId " +
                   $"ORDER BY Id ASC";
        }
    }
}
