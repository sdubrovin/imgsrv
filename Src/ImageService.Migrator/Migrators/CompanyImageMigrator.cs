using System.Collections.Generic;

namespace Radario.ImageService.Migrator {
    public class Company: BaseEntity {
        public string Logo { get; set; }
        public string TicketLogo { get; set; }
    }

    public class CompanyImageMigrator : BaseImageMigrator<Company> {
        public CompanyImageMigrator(string connectionString, ImageUploader imageUploader): base(nameof(Company), connectionString, imageUploader) {}

        protected override IEnumerable<RadarioImage> ExtractImage(IEnumerable<Company> entities) {
            var images = new List<RadarioImage>();

            images.AddRange(
                GetImages("Company.Logo", entities, e => ExtractOneImage(e.Logo))
            );
            images.AddRange(
                GetImages("Company.TicketLogo", entities, e => ExtractOneImage(e.TicketLogo))
            );

            return images;
        }

        protected override string GetEntititesQuery(int fromId, int count) {
            return $"SELECT TOP (@count) Id, Logo, TicketLogo FROM [Company] " +
                   $"WHERE Id > @fromId " +
                   $"ORDER BY Id ASC";
        }
    }
}
