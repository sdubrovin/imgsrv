using System.Collections.Generic;

namespace Radario.ImageService.Migrator {
    public class Event: BaseEntity {
        public string Images { get; set; }
        public string BackgroundImage { get; set; }
        public string PromoImages { get; set; }
        public string PlaceSchemeImage { get; set; }
    }

    public class EventImageMigrator : BaseImageMigrator<Event> {
        public EventImageMigrator(string connectionString, ImageUploader imageUploader): base(nameof(Event), connectionString, imageUploader) { }

        private protected override int PageSize => 5000; 

        protected override IEnumerable<RadarioImage> ExtractImage(IEnumerable<Event> entities) {
            var images = new List<RadarioImage>();

            images.AddRange(
                GetImages("Event.Images", entities, e => ExtractImageList(e.Images))
            );
            images.AddRange(
                GetImages("Event.BackgroundImage", entities, e => ExtractOneImage(e.BackgroundImage))
            );
            images.AddRange(
                GetImages("Event.PromoImages", entities, e => ExtractImageList(e.PromoImages))
            );
            images.AddRange(
                GetImages("Event.PlaceSchemeImage", entities, e => ExtractOneImage(e.PlaceSchemeImage))
            );

            return images;
        }

        protected override string GetEntititesQuery(int fromId, int count) {
            return $"SELECT TOP (@count) Id, Images, BackgroundImage, PromoImages, PlaceSchemeImage FROM [Event] " +
                   $"WHERE Id > @fromId " +
                   $"ORDER BY Id ASC";
        }
    }
}
