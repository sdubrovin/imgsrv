using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Dapper;

namespace Radario.ImageService.Migrator {
    public class BaseEntity {
        public int Id { get; set; }
    }

    public abstract class BaseImageMigrator<T> : IImageMigrator where T : BaseEntity {
        private static readonly JsonSerializerOptions JsonSerializerOptions = new JsonSerializerOptions {
            PropertyNameCaseInsensitive = true
        };
        private readonly string _entityName;
        private readonly ImageUploader _imageUploader;
        private readonly string _connectionString;
        private protected virtual int PageSize => 2000;

        protected BaseImageMigrator(string entityName, string connectionString, ImageUploader imageUploader) {
            _entityName = entityName;
            _connectionString = connectionString;
            _imageUploader = imageUploader;
        }

        public virtual async Task<MigrationReport> Migrate() {
            int id = 0;
            IEnumerable<T> entities;

            Console.WriteLine($"Start {_entityName} migration.");
            var report = new MigrationReport(_entityName);
            report.Start();

            do {
                entities = GetEntitites(id, PageSize);

                var images = ExtractImage(entities);
                await _imageUploader.Upload(images, report);

                id = entities.Max(e => e.Id);

                Console.WriteLine($"{id} {_entityName} migrated");

            } while (entities.Count() == PageSize);

            report.Stop();

            return report;
        }

        private IEnumerable<T> GetEntitites(int fromId, int count) {
            string query = GetEntititesQuery(fromId, count);

            using var conn = new SqlConnection(_connectionString);
            conn.Open();
            return conn.Query<T>(query, new { fromId, count });
        }

        protected abstract string GetEntititesQuery(int fromId, int count);

        protected abstract IEnumerable<RadarioImage> ExtractImage(IEnumerable<T> entities);

        protected IEnumerable<RadarioImage> GetImages(
           string imageType,
           IEnumerable<T> entities,
           Func<T, IEnumerable<RadarioImage>> imageExtractor
       ) {
            var images = entities.SelectMany(e => {
                try {
                    var images = imageExtractor(e);
                    foreach (var img in images) {
                        img.EntityId = e.Id;
                        img.Type = imageType;
                    }
                    return images;
                } catch {
                    //Write to report
                    return Enumerable.Empty<RadarioImage>();
                }
            })
            .Distinct()
            .ToList();

            return images;
        }

        protected IEnumerable<RadarioImage> ExtractImageList(string value) {
            if (string.IsNullOrWhiteSpace(value)) {
                return Enumerable.Empty<RadarioImage>();
            }

            return JsonSerializer.Deserialize<List<RadarioImage>>(value, JsonSerializerOptions);
        }

        protected IEnumerable<RadarioImage> ExtractOneImage(string value) {
            if (string.IsNullOrWhiteSpace(value)) {
                return Enumerable.Empty<RadarioImage>();
            }

            var img = JsonSerializer.Deserialize<RadarioImage>(value, JsonSerializerOptions);
            return new[] { img };
        }
    }
}
