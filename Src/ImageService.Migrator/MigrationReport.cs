using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Radario.ImageService.Migrator {
    public class FailedImage {
        public RadarioImage Image { get; set; }
        public string Error { get; set; }
    }

    public class MigrationReport {
        public string Name { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<string> Errors { get; set; } = new List<string>();
        public List<FailedImage> FailedImages { get; set; } = new List<FailedImage>();

        public MigrationReport(string name) {
            Name = name;
        }

        public void Start() {
            BeginDate = DateTime.Now;
        }

        public void Stop() {
            EndDate = DateTime.Now;
        }
        
        public string GetReport() {
            var sb = new StringBuilder();

            sb.AppendLine($"Report name: {Name}");
            sb.AppendLine($"BeginDate: {BeginDate:s}");
            sb.AppendLine($"EndDate: {EndDate:s}");

            if (FailedImages.Any()) {
                sb.AppendLine("*** Failed images ***");
                sb.AppendLine($"Failed images count: {FailedImages.Count}");
                sb.AppendLine();
                FailedImages.ToList().ForEach(f => {
                    sb.AppendLine($"EntityId: {f.Image.EntityId}");
                    sb.AppendLine($"Type: {f.Image.Type}");
                    sb.AppendLine($"ImageId: {f.Image.Id}");
                    sb.AppendLine($"ImageUrl: {f.Image.Url}");
                    sb.AppendLine($"Error: {f.Error}");
                    sb.AppendLine("---");
                });
            } else {
                sb.AppendLine("No failed images.");
            }

            return sb.ToString();
        }

        public void WritToFile(string path) {
            //Path.GetDirectoryName(dirPath)
            File.WriteAllText(path, GetReport());
        }
    }
}
