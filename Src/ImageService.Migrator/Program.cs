using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Radario.ImageService.Migrator {
    class Program {
        const string ConnectionString = "Data Source = (local); Initial Catalog = Radario; Integrated Security = true;  Connection Lifetime=30";
        const string ImageServiceUri = "http://localhost:7000";

        static async Task Main(string[] args) {
            var httpClient = new HttpClient {
                BaseAddress = new Uri(ImageServiceUri),
                Timeout = TimeSpan.FromMinutes(5)
            };
            var imageUploader = new ImageUploader(httpClient);
            List<IImageMigrator> imageMigrators = new List<IImageMigrator> {
                //new CompanyImageMigrator(ConnectionString, imageUploader),
                //new EventTemplateMigrator(ConnectionString, imageUploader),
                new EventImageMigrator(ConnectionString, imageUploader)
            };

            foreach (var migrator in imageMigrators) {
                var report = await migrator.Migrate();
                var reportPath = Path.Combine(
                    AppDomain.CurrentDomain.BaseDirectory,
                    $"report-{report.Name}-{DateTime.Now:HH-mm-ss}.txt"
                );
                report.WritToFile(reportPath);
            }

            Console.WriteLine("Done");
            Console.ReadKey();
        }

        static IEnumerable<RadarioImage> GetImageList() {
            string[] ids = new [] { "" };

            return ids.Select(id => new RadarioImage {
                Id = id
            });
        }
    }
}
