using System;

namespace ImageService.Common {
    public class ImageSize {
        public int Width { get; protected set; }
        public int Height { get; protected set; }

        public ImageSize(int width, int height) {
            if (width <= 0) {
                throw new ArgumentException(nameof(width));
            }
            if (height <= 0) {
                throw new ArgumentException(nameof(height));
            }
            Width = width;
            Height = height;
        }
    }
}
