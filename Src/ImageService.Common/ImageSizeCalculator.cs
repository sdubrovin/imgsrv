using System;

namespace ImageService.Common {
    public class ImageSizeCalculator : IImageSizeCalculator {
        public ImageSize GetImageSize(ImageSize originalSize, ResizeConfig config) {
            double kWidth = (double)config.Width / originalSize.Width;
            double kHeight = (double)config.Height / originalSize.Height;
            double koeff = 1;

            switch (config.Fill) {
                case FillType.UniformToFill:
                    koeff = Math.Max(kWidth, kHeight);
                    break;
                case FillType.Uniform:
                    koeff = Math.Min(kWidth, kHeight);
                    break;
                case FillType.FillWidth:
                    koeff = kWidth;
                    break;
                case FillType.FillHeight:
                    koeff = kHeight;
                    break;
            }

            return new ImageSize(
                (int)(Math.Round(originalSize.Width * koeff)),
                (int)(Math.Round(originalSize.Height * koeff))
            );
        }
    }
}
