using System.Collections.Generic;

namespace ImageService.Common {
    public class ResizeConfig {
        public ResizeConfig() { }
        public ResizeConfig(string name, int width, int height, FillType fill, AlignType align) {
            Name = name;
            Align = align;
            Fill = fill;
            Width = width;
            Height = height;
        }

        public string Name { get; set; }
        public AlignType Align { get; set; }
        public FillType Fill { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public override bool Equals(object obj) {
            return obj is ResizeConfig config &&
                   Name == config.Name &&
                   Align == config.Align &&
                   Fill == config.Fill &&
                   Width == config.Width &&
                   Height == config.Height;
        }

        public override int GetHashCode() {
            var hashCode = -890056011;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Name);
            hashCode = hashCode * -1521134295 + Align.GetHashCode();
            hashCode = hashCode * -1521134295 + Fill.GetHashCode();
            hashCode = hashCode * -1521134295 + Width.GetHashCode();
            hashCode = hashCode * -1521134295 + Height.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(ResizeConfig left, ResizeConfig right) {
            if (left is null ^ right is null) {
                return false;
            }

            return left?.Equals(right) != false;
        }

        public static bool operator !=(ResizeConfig left, ResizeConfig right) {
            return !(left == right);
        }
    }
}
