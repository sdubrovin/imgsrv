namespace ImageService.Common {
    public interface IImageSizeCalculator {
        ImageSize GetImageSize(ImageSize originalSize, ResizeConfig config);
    }
}
