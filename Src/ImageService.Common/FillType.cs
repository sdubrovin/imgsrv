namespace ImageService.Common {
    public enum FillType {
        FillWidth,
        UniformToFill,
        FillHeight,
        Uniform
    }
}
