using ImageService.Domain.Entities;

namespace ImageService.Application.Images.Commands.Shared {
    public class UploadedImage {
        public string Id { get; set; }
        public string Uri { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public UploadedImage(IImage image, string uri) {
            Id = image.Id;
            Height = image.Size.Height;
            Width = image.Size.Width;
            Uri = uri;
        }
    }
}
