using ImageService.Application.Common.Services;
using ImageService.Domain.Entities;
using System;
using System.Collections.Generic;

namespace ImageService.Application.Images.Commands.Shared {
    public class UploadImagesResult {
        private readonly IImageUriService _imageUriService;

        public UploadImagesResult() {}

        public UploadImagesResult(IImageUriService imageUriService) {
            _imageUriService = imageUriService;
        }

        public List<UploadedImage> Images { get; } = new List<UploadedImage>();

        public void AddImage(IImage image) {
            if (image is null) {
                throw new ArgumentNullException(nameof(image));
            }
            Images.Add(new UploadedImage(image, _imageUriService?.GetImageUri(image)));
        }
    }
}
