﻿using System.IO;

namespace ImageService.Application.Images.Commands.UploadImages {
    public class UploadingImage {
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public Stream Stream { get; set; }
    }
}
