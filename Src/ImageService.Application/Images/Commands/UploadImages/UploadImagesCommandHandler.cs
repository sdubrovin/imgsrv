using MediatR;
using ImageService.Application.Common.Services;
using ImageService.Application.Images.Commands.Shared;
using System.Threading;
using System.Threading.Tasks;

namespace ImageService.Application.Images.Commands.UploadImages {
    public class UploadImagesCommandHandler : IRequestHandler<UploadImagesCommand, UploadImagesResult> {
        private readonly IImageService _imageService;
        private readonly IImageUriService _imageUriService;

        public UploadImagesCommandHandler(
            IImageService imageService,
            IImageUriService imageUriService
        ) {
            _imageService = imageService;
            _imageUriService = imageUriService;
        }

        public async Task<UploadImagesResult> Handle(UploadImagesCommand request, CancellationToken cancellationToken) {
            var result = new UploadImagesResult(_imageUriService);

            foreach (var img in request.Images) {
                var savedImage = await _imageService.SaveImage(img.ContentType, img.Stream);
                result.AddImage(savedImage);
            }

            return result;
        }
    }
}
