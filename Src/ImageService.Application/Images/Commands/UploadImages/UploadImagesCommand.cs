﻿using MediatR;
using ImageService.Application.Images.Commands.Shared;
using System.Collections.Generic;

namespace ImageService.Application.Images.Commands.UploadImages {
    public class UploadImagesCommand : IRequest<UploadImagesResult> {
        public IEnumerable<UploadingImage> Images { get; set; }
    }
}
