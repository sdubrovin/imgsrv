using System.Collections.Generic;
using System.Linq;
using ImageService.Application.Common;
using ImageService.Application.Common.Validation;

namespace ImageService.Application.Images.Commands.UploadImages {
    public class UploadImagesCommandValidator : RequestValidator<UploadImagesCommand> {
        public override IEnumerable<string> ValidateRequest(UploadImagesCommand request) {
            var tooBigImages = request.Images.Where(i => i.Stream.Length > CommonConsts.MaximumFileLength);

            var errors = new List<string>();

            if (tooBigImages.Any()) {
                string imageNames = string.Join(", ", tooBigImages.Select(i => i.FileName));
                errors.Add($"Images {imageNames} are too big. Max image size: {CommonConsts.MaximumFileLength} bytes.");
            }

            return errors;
        }
    }
}
