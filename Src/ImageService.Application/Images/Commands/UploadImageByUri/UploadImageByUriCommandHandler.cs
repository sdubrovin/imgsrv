using MediatR;
using ImageService.Application.Common;
using ImageService.Application.Common.Exceptions;
using ImageService.Application.Common.Services;
using ImageService.Application.Images.Commands.Shared;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace ImageService.Application.Images.Commands.UploadImageByUri {
    public class UploadImageByUriCommandHandler : IRequestHandler<UploadImageByUriCommand, UploadedImage> {
        private readonly IImageService _imageService;
        private readonly IImageUriService _imageUriService;
        private readonly IHttpClientFactory _httpClientFactory;

        public UploadImageByUriCommandHandler(
            IImageService imageService,
            IImageUriService imageUriService,
            IHttpClientFactory httpClientFactory
        ) {
            _imageService = imageService;
            _imageUriService = imageUriService;
            _httpClientFactory = httpClientFactory;
        }

        public async Task<UploadedImage> Handle(UploadImageByUriCommand request, CancellationToken cancellationToken) {
            var httpClient = _httpClientFactory.CreateClient();

            using (var response = await httpClient.GetAsync(request.Url)) {
                if (!response.IsSuccessStatusCode) {
                    throw new BadRequestException($"Failed download image by uri: {request.Url}. StatusCode: {response.StatusCode}");
                }

                var contentStream = await response.Content.ReadAsStreamAsync();
                if (contentStream.Length > CommonConsts.MaximumFileLength) {
                    throw new ImageTooBigException(request.Url);
                }

                var image = await _imageService.SaveImage(
                    response.Content.Headers.ContentType.ToString(),
                    contentStream
                );

                return new UploadedImage(image, _imageUriService.GetImageUri(image));
            }
        }
    }
}
