using MediatR;
using ImageService.Application.Images.Commands.Shared;

namespace ImageService.Application.Images.Commands.UploadImageByUri {
    public class UploadImageByUriCommand : IRequest<UploadedImage> {
        public string Url { get; set; }
    }
}
