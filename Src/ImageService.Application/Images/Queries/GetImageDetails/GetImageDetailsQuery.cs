using System.Collections.Generic;
using MediatR;
using ImageService.Application.Images.Queries.Shared;

namespace ImageService.Application.Images.Queries.GetImageDetails {
    public class GetImageDetailsQuery : IRequest<IEnumerable<ImageDetailsVm>> {
        public IEnumerable<string> ImageIds { get; set; }
    }
}
