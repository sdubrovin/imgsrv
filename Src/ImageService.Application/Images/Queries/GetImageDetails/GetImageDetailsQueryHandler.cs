using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ImageService.Application.Common.Repository;
using ImageService.Application.Images.Queries.Shared;

namespace ImageService.Application.Images.Queries.GetImageDetails {
    public class GetImageDetailsQueryHandler :
        IRequestHandler<GetImageDetailsQuery, IEnumerable<ImageDetailsVm>> {
        private readonly IImageRepository _imageRepository;

        public GetImageDetailsQueryHandler(IImageRepository imageRepository) {
            _imageRepository = imageRepository;
        }

        public async Task<IEnumerable<ImageDetailsVm>> Handle(GetImageDetailsQuery request, CancellationToken cancellationToken) {
            var result = new List<ImageDetailsVm>();

            foreach (var id in request.ImageIds.Distinct()) {
                var image = await _imageRepository.GetImageById(id, null);
                if (image != null) {
                    result.Add(new ImageDetailsVm(image));
                }
            }

            return result;
        }
    }
}
