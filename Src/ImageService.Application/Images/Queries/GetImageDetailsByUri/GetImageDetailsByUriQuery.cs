using System.Collections.Generic;
using MediatR;
using ImageService.Application.Images.Queries.Shared;

namespace ImageService.Application.Images.Queries.GetImageDetailsByUri {
    public class GetImageDetailsByUriQuery: IRequest<IEnumerable<ImageDetailsVm>> {
        public IEnumerable<string> ImageUris { get; set; }
    }
}
