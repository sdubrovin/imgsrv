using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ImageService.Application.Common.Repository;
using ImageService.Application.Images.Queries.Shared;

namespace ImageService.Application.Images.Queries.GetImageDetailsByUri {
    public class GetImageDetailsByUriQueryHandler : IRequestHandler<GetImageDetailsByUriQuery, IEnumerable<ImageDetailsVm>> {
        private readonly IImageRepository _imageRepository;

        public GetImageDetailsByUriQueryHandler(IImageRepository imageRepository) {
            _imageRepository = imageRepository;
        }

        public async Task<IEnumerable<ImageDetailsVm>> Handle(GetImageDetailsByUriQuery request, CancellationToken cancellationToken) {
            var ids = request.ImageUris.Select(i => new Uri(i).Segments.Last());

            var result = new List<ImageDetailsVm>();
            foreach (var id in ids.Distinct()) {
                var image = await _imageRepository.GetImageById(id, null);
                if (image != null) {
                    result.Add(new ImageDetailsVm(image));
                }
            }

            return result;
        }
    }
}
