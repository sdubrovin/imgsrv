using ImageService.Domain.Entities;

namespace ImageService.Application.Images.Queries.Shared {
    public class ImageDetailsVm {
        public string Id { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public ImageDetailsVm(IImage image) {
            Id = image.Id;
            Width = image.Size.Width;
            Height = image.Size.Height;
        }
    }
}
