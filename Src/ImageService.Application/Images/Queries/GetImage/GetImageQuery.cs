﻿using MediatR;

namespace ImageService.Application.Images.Queries.GetImage {
    public class GetImageQuery : IRequest<ImageVm> {
        public string ImageId { get; set; }
        public string ResizeConfig { get; set; }
    }
}
