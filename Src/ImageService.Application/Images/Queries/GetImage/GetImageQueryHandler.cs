﻿using MediatR;
using ImageService.Application.Common.Services;
using ImageService.Domain.ValueObjects;
using System.Threading;
using System.Threading.Tasks;

namespace ImageService.Application.Images.Queries.GetImage {
    public class GetImageQueryHandler : IRequestHandler<GetImageQuery, ImageVm> {
        private readonly IImageService _imageService;

        public GetImageQueryHandler(IImageService imageService) {
            _imageService = imageService;
        }

        public async Task<ImageVm> Handle(GetImageQuery request, CancellationToken cancellationToken) {
            var image = await _imageService.GetImage(request.ImageId, (ResizeConfigName)request.ResizeConfig);
            return new ImageVm {
                MimeType = image.MimeType,
                Data = image.Data
            };
        }
    }
}
