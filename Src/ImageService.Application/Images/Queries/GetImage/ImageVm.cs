﻿namespace ImageService.Application.Images.Queries.GetImage {
    public class ImageVm {
        public string MimeType { get; set; }
        public byte[] Data { get; set; }
    }
}
