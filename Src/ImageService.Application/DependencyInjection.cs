using MediatR;
using Microsoft.Extensions.DependencyInjection;
using ImageService.Application.Common.Validation;
using ImageService.Application.Images.Commands.UploadImages;
using System.Reflection;

namespace ImageService.Application {
    public static class DependencyInjection {
        public static IServiceCollection AddApplication(this IServiceCollection services) {
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddTransient(typeof(IRequestValidator<UploadImagesCommand>), typeof(UploadImagesCommandValidator));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationPipelineBehavior<,>));
            return services;
        }
    }
}
