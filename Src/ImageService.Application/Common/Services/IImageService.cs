using ImageService.Domain.Entities;
using ImageService.Domain.ValueObjects;
using System.IO;
using System.Threading.Tasks;

namespace ImageService.Application.Common.Services {
    public interface IImageService {
        Task<IImage> GetImage(ImageId imageId, ResizeConfigName configName);
        Task<IImage> SaveImage(string contentType, Stream stream);
    }
}
