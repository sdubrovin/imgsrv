﻿using ImageService.Common;

namespace ImageService.Application.Common.Services {
    public interface IImageResizer {
        byte[] Resize(byte[] imageBytes, string mimeType, ResizeConfig config);
    }
}