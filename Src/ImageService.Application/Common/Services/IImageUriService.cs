using ImageService.Domain.Entities;

namespace ImageService.Application.Common.Services {
    public interface IImageUriService {
        string GetImageUri(IImage image);
    }
}
