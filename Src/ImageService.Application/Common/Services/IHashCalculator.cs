using System.Threading.Tasks;

namespace ImageService.Application.Common.Services {
    public interface IHashCalculator {
        Task<string> GetHash(byte[] bytes);
    }
}
