namespace ImageService.Application.Common {
    internal static class CommonConsts {
        public const int MaximumFileLength = 1024 * 1024 * 5;
    }
}
