using ImageService.Common;
using ImageService.Domain.Entities;
using ImageService.Domain.ValueObjects;
using System.Threading.Tasks;

namespace ImageService.Application.Common.Repository {
    public interface IImageRepository {
        Task SaveImage(IImage image);
        Task<IImage> GetImageById(ImageId id, ResizeConfig resizeConfig);
        Task<IImage> GetImageByHash(string dataHash);
    }
}
