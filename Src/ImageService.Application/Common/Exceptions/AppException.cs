using System;

namespace ImageService.Application.Common.Exceptions {
    public class AppException : Exception {
        public AppException(string message): base(message) { }
    }
}
