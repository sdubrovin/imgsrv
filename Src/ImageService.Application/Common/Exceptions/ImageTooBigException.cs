namespace ImageService.Application.Common.Exceptions {
    public class ImageTooBigException: BadRequestException {
        public ImageTooBigException(string fileName): base($"Image {fileName} too big. Max image size: {CommonConsts.MaximumFileLength} bytes.") { }
    }
}
