using ImageService.Domain.ValueObjects;

namespace ImageService.Application.Common.Exceptions {
    public class ImageNotFoundException: AppException {
        public ImageNotFoundException(ImageId imageId) : base($"Image {imageId} not found.") { }
    }
}
