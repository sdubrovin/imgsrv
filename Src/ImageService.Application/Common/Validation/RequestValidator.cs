using System.Collections.Generic;

namespace ImageService.Application.Common.Validation {
    public abstract class RequestValidator<TRequest> : IRequestValidator<TRequest> where TRequest: class {
        public IEnumerable<string> Validate(TRequest request) {
            if (request is null) {
                return new[] { "Request is empty." };
            }

            return ValidateRequest(request);
        }

        public abstract IEnumerable<string> ValidateRequest(TRequest request);
    }
}
