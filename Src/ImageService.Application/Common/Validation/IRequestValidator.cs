﻿using System.Collections.Generic;

namespace ImageService.Application.Common.Validation {
    public interface IRequestValidator<TRequest> {
        IEnumerable<string> Validate(TRequest request);
    }
}
