using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ImageService.Application.Common.Exceptions;

namespace ImageService.Application.Common.Validation {
    public class RequestValidationPipelineBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> {
        private readonly IEnumerable<IRequestValidator<TRequest>> _validators;

        public RequestValidationPipelineBehavior(IEnumerable<IRequestValidator<TRequest>> validators) {
            _validators = validators;
        }

        public Task<TResponse> Handle(
            TRequest request,
            CancellationToken cancellationToken,
            RequestHandlerDelegate<TResponse> next) {

            var errors = _validators
                .SelectMany(v => v.Validate(request))
                .ToList();

            if (errors.Any()) {
                var errorString = $"Errors: {string.Join(", ", errors)}";
                throw new BadRequestException(errorString);
            }

            return next();
        }
    }
}
